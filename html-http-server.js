// import the http module
const http = require("http");

try {
  // create the server by using http
  const server = http.createServer((request, response) => {
    // check the http method and url
    if (request.method == "GET" && request.url == "/html") {
      response.statusCode = 200;
      // set the header
      response.setHeader("content-type", "text/html");
      // html code
      response.write(
        `<html>
               <head>
               </head>
               <body>
                   <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                   <p> - Martin Fowler</p>
             
               </body>
             </html>`
      );
      // end the server
      response.end();
    } else {
      response.statusCode = 404;
      response.write("Not Found");
      response.end();
    }
  });
  // sever listener
  server.listen(8080, () => {
    console.log("Sever Runing...!");
  });
} catch (error) {
  console.log(error);
}
